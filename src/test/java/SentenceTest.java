import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

    class SentenceTest {

        @Test
        @DisplayName("Standard input")
        void countWords() {
            Sentence sent = new Sentence("How are you");
            String result = sent.countWords().toString();
            assertEquals("{are=1, how=1, you=1}", result);
        }

        @Test
        @DisplayName("Repeated input")
        void countWords2() {
            Sentence sent = new Sentence("How how how");
            String result = sent.countWords().toString();
            assertEquals("{how=3}", result);
        }

        @Test
        @DisplayName("Empty input")
        void countWords3() {
            Sentence sent = new Sentence("");
            String result = sent.countWords().toString();
            assertEquals("{}", result);
        }

        @Test
        @DisplayName("Number input")
        void countWords4() {
            Sentence sent = new Sentence("23 42 88 23");
            String result = sent.countWords().toString();
            assertEquals("{23=2, 42=1, 88=1}", result);
        }

        @Test
        @DisplayName("Unexpected input")
        void countWords5() {
            Sentence sent = new Sentence("!! , .");
            String result = sent.countWords().toString();
            assertEquals("{}", result);
        }
    }
