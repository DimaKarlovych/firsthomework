import java.util.*;
import static java.util.stream.Collectors.toMap;

public class FirstHomework {
    public static void main(String[] args) {
        Sentence sent = new Sentence("How are you? I am fine, thanks. I should go, see you.");
        System.out.println(sent.countWords());
    }
}

class Sentence {
    String text;

    Sentence(String text) {
        this.text = text;
    }

    Map<String,Integer> countWords() {
        String []data = text.toLowerCase().split("[ ,.!\\?]");
        HashMap<String,Integer> unsorted = new HashMap<>();
        Map<String,Integer>sortedByKey;
        Integer count = 0;
        for(String word : data) {
            if(!word.equals(" ") && !word.equals("")) {
                count = unsorted.get(word);
                if(count == null) {
                    count = 0;
                }
                unsorted.put(word,count+1);
            }
        }

        sortedByKey = unsorted
                .entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer> comparingByKey())
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        return sortedByKey;
    }
}

