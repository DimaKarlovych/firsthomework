# Simple Java project using Maven Framework
Application that counts words in sentences and prints quantity of each.
### Build
To build this project you must type next command:
```
mvn clean compile assembly:single
```

### Execution
To execute project, first you should build it, then do next command in
the folder which called 'target'
```
java -jar Second-1.0-SNAPSHOT-jar-with-dependencies.jar
```
